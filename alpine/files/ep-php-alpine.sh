#!/bin/bash

function _echo()
{
	[ "${DEBUG}" == "true" ] && echo "$@"
}

echo "
[program:phpfpm]
command=php-fpm -F

" > /etc/supervisord.d/php.ini

# Prepare PHP var directories
install -d -o www-data -g www-data -m 770 /var/log/php /var/lib/php/fpm /var/lib/php/fpm/{session,wsdlcache,opcache}

# PHP.ini
ln -s /usr/local/etc/php/php.ini-production /usr/local/etc/php/php.ini

# TUNING CLI
#cat <<END > /usr/local/etc/php/conf.d/99-tuning.ini
#memory_limit = ${CLI_PHP_MEMORY_LIMIT:-128M}
#upload_max_filesize = ${CLI_PHP_UPLOAD_MAX_FILESIZE:-20M}
#post_max_size = ${CLI_PHP_POST_MAX_SIZE:-25M}
#END

# TUNING (CLI + PHP-FPM)
set | grep "^PHP__" | while read L ; do
 echo "${L#PHP__}" | sed -e 's,_dot_,.,' -e 's,=, = ,' 
done > /tmp/tuning.ini

# Apply PHP config
mv /tmp/tuning.ini /usr/local/etc/php/conf.d/99-tuning.ini


_echo -e "TUNING PHP-CLI (/usr/local/etc/php/conf.d/99-tuning.ini)" :
[ "${DEBUG}" == "true" ] && cat /usr/local/etc/php/conf.d/99-tuning.ini

# TUNING POOL PHP-FPM
_echo -e "\n======================================="
_echo -e "Tuning PHP-FPM:"
_echo -e "==============="
_echo -e "** PM_MODE : ${PM_MODE:=ondemand}"
_echo -e "** PM_MAX_CHILDREN : ${PM_MAX_CHILDREN:=10}"
_echo -e "** PM_MAX_REQUESTS : ${PM_MAX_REQUESTS:=500}"
_echo -e "** PM_PROCESS_IDLE_TIMEOUT : ${PM_PROCESS_IDLE_TIMEOUT:=30s}"
_echo -e "** PHP_MEMORY_LIMIT        : ${PHP_MEMORY_LIMIT:=256M}"
_echo -e "** PHP_UPLOAD_MAX_FILESIZE : ${PHP_UPLOAD_MAX_FILESIZE:=20M}"
_echo -e "** PHP_POST_MAX_SIZE       : ${PHP_POST_MAX_SIZE:=25M}"
_echo -e "======================================="

sed -i \
    -e "/^group =/s,WEB_GROUPNAME,www-data," \
    -e "s,PM_MODE,${PM_MODE}," \
    -e "s,PM_MAX_CHILDREN,${PM_MAX_CHILDREN}," \
    -e "s,PM_PROCESS_IDLE_TIMEOUT,${PM_PROCESS_IDLE_TIMEOUT}," \
    -e "s,PM_MAX_REQUESTS,${PM_MAX_REQUESTS}," \
    -e "s,PHP_MEMORY_LIMIT,${PHP_MEMORY_LIMIT}," \
    -e "s,PHP_UPLOAD_MAX_FILESIZE,${PHP_UPLOAD_MAX_FILESIZE}," \
    -e "s,PHP_POST_MAX_SIZE,${PHP_POST_MAX_SIZE}," \
    ${PHPFPM_POOL_CONFIG}

# Tuning des processus uniquement si MODE = dynamic
if [ "${PM_MODE}" == "dynamic" ] ; then
  _echo -e "** PM_START_SERVERS : ${PM_START_SERVERS:=2}"
  _echo -e "** PM_MIN_SPARE_SERVERS : ${PM_MIN_SPARE_SERVERS:=2}"
  _echo -e "** PM_MAX_SPARE_SERVERS : ${PM_MAX_SPARE_SERVERS:=5}"
else
  PM_START_SERVERS=0
  PM_MIN_SPARE_SERVERS=0
  PM_MAX_SPARE_SERVERS=0
fi
sed -i \
    -e "s,PM_START_SERVERS,${PM_START_SERVERS}," \
    -e "s,PM_MIN_SPARE_SERVERS,${PM_MIN_SPARE_SERVERS}," \
    -e "s,PM_MAX_SPARE_SERVERS,${PM_MAX_SPARE_SERVERS}," \
    ${PHPFPM_POOL_CONFIG}

_echo -e "\nPHP-FPM pool config : (${PHPFPM_POOL_CONFIG})"
_echo -e "==================="
# grep -vE '^;|^ *$' ${PHPFPM_POOL_CONFIG}

