# Image Docker HTTPD/PHP

## composants

  - Apache HTTPD 2.4 
  - PHP : 7.4 à 8.3
  - Composer (latest v1 & latest v2)

## Environement

Les variables suivantes sont passées au conteneur 


### Permisisons / propriéraire

Si la variable **FIX_WEBCNONTENT_OWNER** est positionnée à true, le propriétaire et le groupe des fichiers montés dans /var/www/html sera positionné à www-data.www-data pour assurer le droit de lecture/écriture par le serveur HTTP & PHP. Non définie par défaut.

### Systeme


  - "LDAP_TLS_REQCERT" : valeur affectée à TLS_REQCERT dans /etc/opendldap/ldap.conf (par défaut : **never**)

  - LANG : valeur par défaut = **fr_FR.UTF-8**

  - TIMZEONE : valeur par défaut = **Europe/Paris**
  
      - Effectif au niveau OS + au niveau de PHP.


### HTTPD

  - HTTPD_MPM : choix du MPM d'Apache (prefork, worker, **event**)

  - HTTPD_ENABLE_CGI : Activer CGI ou non (**false**)

#### Status : mod_status

  - HTTPD_ENABLE_STATUS : activation (si true) du module mod_status

  - HTTPD_STATUS_URI : l'URI (par défaut : /status)

  - HTTPD_STATUS_ALLOWED_IP : liste des adresses IP depuis lesquelles on peut accéder au status

    - Par défaut : seule 127.0.0.1 est autorisée (status inaccessible publiquement)

    - Pour autoriser toute adresse : HTTPD_STATUS_ALLOWED_IP=any

    - Pour autoriser plusieurs réseaux : HTTPD_STATUS_ALLOWED_IP="192.168.122 172.16. 10.0.0.0/8"



#### Interface avec PHP-FPM

PHP-FPM est contactable par le port tcp/9000 de localhost.

On peut désactiver le proxypassing vers PHP-FPM en positionnant la variable **HTTPD_ENABLE_PHPFPM_INET** à **false**. Il est cependant préférable d'utiliser l'image "httpd" et non pas "httpd-php" si l'on ne souhaite pas activer PHP-FPM...


#### Contenu à servir

  - WEB_APP_DIR : sous-répertoire du **DocumentRoot** à servir par le serveur.

      valeurs usuelles :

      - **public**  (Symfony 4), 

      - **web** (Symfony 3.4), 

      valeur par défaut = **vide** --> DocumentRoot = /var/www/html


  - WEB_APP_INDEX : nom de l'index (**index.php**, app.php, ...)


### PHP

Configuration globale php.ini dans /usr/local/etc/php/php.ini

Ce n'est qu'un lien symbolique vers le fichier standard /usr/local/etc/php/php.ini-production (sans modification)

#### Tuning local

Toute variable d'environnement préfixée par **PHP__** est intégrée en directive de configuration de PHP dans le fichier "**99-tuning.ini**" au démarrage du conteneur.

Ce fichier de configuration "**99-tuning.ini**" est automatiquement ajouté dans **/usr/local/etc/php/conf.d** (base alpine) ou **/etc/php.d** (base CentOS) et donc pris en compte dans tous les cas.

Exemple : 

```
docker container run --rm -it --env PHP__memory_limit=128M  phpfpm:7.3-centos cat /etc/php.d/99-tuning.ini
```

Les variables dont le nom contient un "." (exemple session.save_path) doivent être codées en remplaçant le "." par "**underscore**" "**dot**" "**underscore**" :

Exemple :

    PHP__session_dot_save_path=valeur


Exemple de déclarations :
```shell
$ cat env.txt 
PHP__post_max_size=50M
PHP__memory_limit=256M
PHP__upload_max_filesize=29M
PHP__session_dot_save_path=/var/lib/php/fpm/session
```


#### PHP-CLI

En complément, un fichier de configuration "99-tuning.ini" est ajouté dans /usr/local/etc/php/conf.d

Il porte les variables définies pour la CLI php.

  - **CLI_PHP_MEMORY_LIMIT** --> memory_limit (par défaut = 128M)
  - **CLI_PHP_UPLOAD_MAX_FILESIZE** --> upload_max_filesize (par défaut = 10M)


#### PHP-FPM

Le pool PHP-FPM a son propre fichier de paramétragei et peut donc porter des valeurs différentes de celles du CLI.

Les variables suivantes sont prises en compte :

  - **PHP_MEMORY_LIMIT** --> memory_limit (par défaut = 256M)
  - **PHP_UPLOAD_MAX_FILESIZE** --> upload_max_FILESIZE (par défaut = 20M)
  - **PHP_POST_MAX_SIZE** --> post_max_size (par défaut = 25M)

Tuning du pool PHP-FPM

  - **PM_MODE** : **ondemand** / dynamic / static).
  - **PM_MAX_CHILDREN** : par défaut 10.
  - **PM_MAX_REQUESTS** : par défaut 500.
  - **PM_PROCESS_IDLE_TIMEOUT** : par défaut 30s

Si PM_MODE = dynamic :

  - **PM_START_SERVERS** : par défaut 2
  - **PM_MIN_SPARE_SERVERS** : par défaut 2
  - **PM_MAX_SPARE_SERVERS** : par défaut 5




## Exemples d'utiliation basique


Création d'un projet Symfony 4 :

```
docker container run -it --rm -v ${PWD}:/var/www/html   httpd-php bash
# composer create-project symfony/website-skeleton appli
```

Pour le développement : lancement avec le serveur de dev de PHP :

```
$ docker container run --rm -dit --name web --hostname web.local -v /srv/web/dev:/var/www/html  httpd-php php -S web:80
$ echo "<?php  phpinfo(); ?>" > /srv/web/dev/index.php
```


Pour la production, lancement avec le serveur HTTPD proxy:fcgi -> PHP-FPM

```
docker container run -d --rm --name web --hostname web.local -v /srv/web/dev:/var/www/html -p 80:80 httpd-php
```

# Créer votre propre image pour votre application


## HTTPD Vhost

C'est la déclaration du vhost fournie par défaut.

````
$ cat config/httpd-vhost.conf
<VirtualHost *:80>
    DocumentRoot /var/www/html/WEB_APP_DIR

    <Directory /var/www/html>
        Options FollowSymlinks
        AllowOverride All
        Require all granted
    </Directory>


    <FilesMatch "\.php$">
        SetHandler "proxy:fcgi://127.0.0.1:9000"
    </FilesMatch>


    # All logs in /httpdlogs/, with auto rotatelogs
    # ! Debian : /usr/bin/rotatelogs, CentOS/Alpine: /usr/sbin/rotatelogs
    ErrorLog "|/usr/sbin/rotatelogs -l -L /httpdlogs/error_log /httpdlogs/%Y%m%d-error_log 86400"

    # Gestion des logs : cas "normal" ou "proxy"
    LogFormat "%h %l %u %t \"%r\" %>s %b \"%{Referer}i\" \"%{User-Agent}i\"" combined
    LogFormat "%{X-Forwarded-For}i %l %u %t \"%r\" %>s %b \"%{Referer}i\" \"%{User-Agent}i\"" proxy

    SetEnvIf X-Forwarded-For "^.*\..*\..*\..*" forwarded

    CustomLog "|/usr/sbin/rotatelogs -l -L /httpdlogs/access_log /httpdlogs/%Y%m%d-access_log 86400" proxy    env=forwarded
    CustomLog "|/usr/sbin/rotatelogs -l -L /httpdlogs/access_log /httpdlogs/%Y%m%d-access_log 86400" combined env=!forwarded

</VirtualHost>
````

## PHP

### Modules

Pour ajouter des modules à votre propre image :

```
FROM httpd-php

# À ajouter dans les images spécifiques pour les applications : FROM httpd-php, puis ...
# PostGresql ?
RUN set -ex \
 && apk add --update git postgresql-dev icu-dev \
 && docker-php-ext-install \
         pgsql pdo_pgsql \
 && rm -rf /tmp/* /var/cache/apk/*

# Intl, mbstring, sockets  ?
RUN set -ex \
 && apk add --update icu-dev \
 && docker-php-ext-install \
         intl \
         mbstring \
         sockets \
 && rm -rf /tmp/* /var/cache/apk/*

# Autres extensions... qui pourraient être nécessaires
# imap,ldap,soap,process
```


### PHP-FPM Pool

C'est la configuration du pool "www" fourni par défaut.

````
$ grep -vE '^;|^$' config/phpfpm-pool-www.conf 
...

````

## Logs

Leur rotation est automatique et ils sont stockés dans (un volume) /httpdlogs dans le conteneur.
-> Voir conf vhost et pool fpm

